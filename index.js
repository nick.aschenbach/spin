const {execSync} = require('child_process')
const jimp = require('jimp')

const run = async () => {
  execSync('rm -rf ./output')
  execSync('mkdir output')
  for(let i = 0; i < 360; i++) {
    const file = await jimp.read('./fractal.jpg')
    const modifiedFile = await file.color([{ apply: 'spin', params: [i]}])
    const padded = i.toString().padStart(3, '0')
    await modifiedFile.write(`./output/${padded}.jpg`)

    process.stdout.write('.')
    if(i % 10 === 9) console.log(`\n${(100.0 * i / 360).toFixed(1)}% complete`)
  }
}

run()