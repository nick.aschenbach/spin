spin
----

Creates a series of color shifted images based on an initial images that can be stitched together into a video. 

Run the app (takes a few minutes)

```
node index.js
```

Create a video from the output frames using `ffmpeg`:

```
ffmpeg -i ./output/%03d.jpg  out.mp4
```

Create it with a sound track (pick something ambient):

```
ffmpeg  -i ./output/%03d.jpg -i audio_file.mp3 -shortest out.mp4
```

